
| Playbook                                         | Description                                                 |
| ------------------------------------------------ | ----------------------------------------------------------- |
| `cpanel_up.yml`                                  | Update cpanel |
| `csf.yml`                                        | Update CSF |
| `lsup.yml`                                       | Update LightSpeed |
| `yum_updates.yml`                                | Update Centos and Redhat server without kernel modules |
| `rootkit.yml`                                | Install rkhunter and chkrootkit |
| `nrpe.yml`                                | Install nrpe |
| `jetapi.yml`                                | Jetapi |